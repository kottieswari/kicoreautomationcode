package com.ki.interactions;

import java.sql.Timestamp;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;









import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import com.google.common.base.Function;
import com.ki.driverscript.DriverScriptRef;
import com.ki.driverscript.Environment;
import com.ki.objectrepository.ObjectRepository;
import com.ki.utils.Log;
import com.relevantcodes.extentreports.LogStatus;

public class Interactions2 {

	// This method is used to create a webelemenet object with reference to the
	// ORPage Name and the Field Name
	public WebElement getWebElement(WebDriver driver, String strScreenName,
			String strLocatorReference) {

		String identifyBy = ObjectRepository.getORdata(strScreenName,
				strLocatorReference, "LocatorType");
		String locator = ObjectRepository.getORdata(strScreenName,
				strLocatorReference, "Locator");

		return webElement(driver, identifyBy, locator);

	}

	// This method is used to create a webelemenet object with reference to the
	// ORPage Name and the Field Name inside the specified webelement
	public WebElement getWebElement(WebDriver driver, WebElement welement,
			String strScreenName, String strLocatorReference) {

		String identifyBy = ObjectRepository.getORdata(strScreenName,
				strLocatorReference, "LocatorType");
		String locator = ObjectRepository.getORdata(strScreenName,
				strLocatorReference, "Locator");

		return webElement(driver, welement, identifyBy, locator);

	}

	// This method is used to create a webelemenets object with reference to the
	// ORPage Name and the Field Name
	public List<WebElement> getWebElements(WebDriver driver,
			String strScreenName, String strLocatorReference) {

		String identifyBy = ObjectRepository.getORdata(strScreenName,
				strLocatorReference, "LocatorType");
		String locator = ObjectRepository.getORdata(strScreenName,
				strLocatorReference, "Locator");

		return webElements(driver, identifyBy, locator);

	}

	// This method is used to create a webelemenets object with reference to the
	// ORPage Name and the Field Name inside the specified webelement
	public List<WebElement> getWebElements(WebDriver driver,
			WebElement welement, String strScreenName,
			String strLocatorReference) {

		String identifyBy = ObjectRepository.getORdata(strScreenName,
				strLocatorReference, "LocatorType");
		String locator = ObjectRepository.getORdata(strScreenName,
				strLocatorReference, "Locator");

		return webElements(driver, welement, identifyBy, locator);

	}

	// constructs the webelement object with references to the locator and
	// identification type.
	private List<WebElement> webElements(WebDriver driver, WebElement welement,
			String identifyBy, String locator) {

		List<WebElement> we = null;

		// System.out.println("in webElment..local driver"+driver+"  global "+DriverScriptRef.driver);
		try {

			if (identifyBy.equalsIgnoreCase("xpath")) {
				we = welement.findElements(By.xpath(locator));
			} else if (identifyBy.equalsIgnoreCase("id")) {
				we = welement.findElements(By.id(locator));
			} else if (identifyBy.equalsIgnoreCase("name")) {
				we = welement.findElements(By.name(locator));
			} else if (identifyBy.equalsIgnoreCase("cssselector")) {
				we = welement.findElements(By.cssSelector(locator));
			} else if (identifyBy.equalsIgnoreCase("classname")) {
				we = welement.findElements(By.className(locator));
			} else if (identifyBy.equalsIgnoreCase("linktext")) {
				we = welement.findElements(By.linkText(locator));
			} else if (identifyBy.equalsIgnoreCase("partiallinktext")) {
				we = welement.findElements(By.partialLinkText(locator));
			} else if (identifyBy.equalsIgnoreCase("tagname")) {
				we = welement.findElements(By.tagName(locator));
			} 

		} catch (Exception e) {

			return null;
		}
		return we;
	}

	// constructs the webelements object with references to the locator and
	// identification type.
	private List<WebElement> webElements(WebDriver driver, String identifyBy,
			String locator) {

		List<WebElement> we = null;

		// System.out.println("in webElment..local driver"+driver+"  global "+DriverScriptRef.driver);
		try {

			if (identifyBy.equalsIgnoreCase("xpath")) {
				we = driver.findElements(By.xpath(locator));
			} else if (identifyBy.equalsIgnoreCase("id")) {
				we = driver.findElements(By.id(locator));
			} else if (identifyBy.equalsIgnoreCase("name")) {
				we = driver.findElements(By.name(locator));
			} else if (identifyBy.equalsIgnoreCase("cssselector")) {
				we = driver.findElements(By.cssSelector(locator));
			} else if (identifyBy.equalsIgnoreCase("className")) {
				we = driver.findElements(By.className(locator));
			} else if (identifyBy.equalsIgnoreCase("linktext")) {
				we = driver.findElements(By.linkText(locator));
			} else if (identifyBy.equalsIgnoreCase("partiallinktext")) {
				we = driver.findElements(By.partialLinkText(locator));
			} else if (identifyBy.equalsIgnoreCase("tagname")) {
				we = driver.findElements(By.tagName(locator));

			}

		} catch (Exception e) {

			return null;
		}
		return we;
	}

	// constructs the webelement object with references to the locator and
	// identification type within the webelement reference
	private WebElement webElement(WebDriver driver, WebElement welement,
			String identifyBy, String locator) {

		WebElement we = null;

		// System.out.println("in webElment..local driver"+driver+"  global "+DriverScriptRef.driver);
		try {

			if (identifyBy.equalsIgnoreCase("xpath")) {
				we = welement.findElement(By.xpath(locator));
			} else if (identifyBy.equalsIgnoreCase("id")) {
				we = welement.findElement(By.id(locator));
			} else if (identifyBy.equalsIgnoreCase("name")) {
				we = welement.findElement(By.name(locator));
			} else if (identifyBy.equalsIgnoreCase("cssselector")) {
				we = welement.findElement(By.cssSelector(locator));
			} else if (identifyBy.equalsIgnoreCase("className")) {
				we = welement.findElement(By.className(locator));
			} else if (identifyBy.equalsIgnoreCase("linktext")) {
				we = welement.findElement(By.linkText(locator));
			} else if (identifyBy.equalsIgnoreCase("partiallinktext")) {
				we = welement.findElement(By.partialLinkText(locator));
			} else if (identifyBy.equalsIgnoreCase("tagname")) {
				we = welement.findElement(By.tagName(locator));

			}

			
			
			if(we.getTagName().trim().contentEquals("input")){
				String strTypeOfElement =we.getAttribute("type");
				if(strTypeOfElement.contentEquals("button") || strTypeOfElement.contentEquals("radio") || strTypeOfElement.contentEquals("checkbox"))
				{
					we= (WebElement) (new WebDriverWait(driver,	30)).until(ExpectedConditions.elementToBeClickable(we));
				}
				else {
					we= (WebElement) (new WebDriverWait(driver,	30)).until(ExpectedConditions.visibilityOf(we));		
				}
			}
			else if(we.getTagName().trim().contentEquals("a") || we.getTagName().trim().contentEquals("button")){
				we= (WebElement) (new WebDriverWait(driver,	30)).until(ExpectedConditions.elementToBeClickable(we));
			}

			else{
				
				we= (WebElement) (new WebDriverWait(driver,	30)).until(ExpectedConditions.visibilityOf(we));
			}
			
			
		} 
		catch (StaleElementReferenceException e) {
			Log.info("Stale Exception in identifying the element");
			return null;
		}
		catch (WebDriverException e) {
			Log.info("WebDriver Exception in identifying the element");
			return null;
		}
		catch (ElementNotFoundException e) {
			Log.info("Element Not found Exception in identifying the element");
			return null;
		}
		catch (Exception e) {
			Log.info("Exception in identifying the element");
			return null;
		}
		return we;
	}

	// constructs the webelement object with references to the locator and
	// identification type.
	private WebElement webElement(WebDriver driver, String identifyBy,
			String locator) {

		WebElement we = null;

		// System.out.println("in webElment..local driver"+driver+"  global "+DriverScriptRef.driver);
		try {

			if (identifyBy.equalsIgnoreCase("xpath")) {
				we = driver.findElement(By.xpath(locator));
			} else if (identifyBy.equalsIgnoreCase("id")) {
				we = driver.findElement(By.id(locator));
			} else if (identifyBy.equalsIgnoreCase("name")) {
				we = driver.findElement(By.name(locator));
			} else if (identifyBy.equalsIgnoreCase("cssselector")) {
				we = driver.findElement(By.cssSelector(locator));
			} else if (identifyBy.equalsIgnoreCase("className")) {
				we = driver.findElement(By.className(locator));
			} else if (identifyBy.equalsIgnoreCase("linktext")) {
				we = driver.findElement(By.linkText(locator));
			} else if (identifyBy.equalsIgnoreCase("partiallinktext")) {
				we = driver.findElement(By.partialLinkText(locator));
			} else if (identifyBy.equalsIgnoreCase("tagname")) {
				we = driver.findElement(By.tagName(locator));
			}

			
			we= (WebElement) (new WebDriverWait(driver,	30)).until(ExpectedConditions.visibilityOf(we));
			
			
		} catch (Exception e) {
			return null;
		}
		return we;
	}

	// Click on any object
	public boolean click(WebDriver driver, String identifyBy, String locator) throws InterruptedException {
		// System.out.println("in click..local driver"+driver+"  global "+DriverScriptRef.driver);
		WebElement we = webElement(driver, identifyBy, locator);
	
		if (we == null) {
			Log.info("Not Found-> " + identifyBy + " using Locator-> "
					+ locator);
			return false;
		} else {
			Log.info("Found-> " + identifyBy + " using Locator-> " + locator);
			we.click();
			Thread.sleep(2000);
			Log.info("Clicked on -> " + identifyBy + " Locator-> " + locator);
			return true;
		}

	}

	// Click on Link, additional properties
	public boolean clickLink(WebDriver driver, String identifyBy, String locator) throws InterruptedException {

		WebElement we = webElement(driver, identifyBy, locator);
		if (we == null) {
			Log.info("Not Found-> " + identifyBy + " using Locator-> "
					+ locator);
			return false;
		} else {
			Log.info("Found-> " + identifyBy + " using Locator-> " + locator);
			we.click();
			Thread.sleep(2000);
			Log.info("Clicked on -> " + identifyBy + " Locator-> " + locator);
			return true;
		}

	}

	// Enter value in text box
	public boolean enterValue(WebDriver driver, String identifyBy,
			String locator, String valuetoType) throws InterruptedException {

		WebElement we = webElement(driver, identifyBy, locator);
		if (we == null) {
			Log.info("Not Found-> " + identifyBy + " using Locator-> "
					+ locator);
			return false;
		} else {
			Log.info("Found-> " + identifyBy + " using Locator-> " + locator);
			we.clear();
			we.sendKeys(valuetoType);
			Thread.sleep(2000);
			Log.info("Entered Value on -> " + identifyBy + " Locator-> "
					+ locator);
			return true;
		}

	}
	

	// This is used to upload the file .
	/*public boolean fileUpload(WebDriver driver, String identifyBy,
			String locator, String valuetoType) {

		try {
			Log.info("Uploading the file " + valuetoType);
			AutoItX x = new AutoItX();
			// x.run("calc.exe");
			String strTitleClassName = "[CLASS:#32770]";
			x.winWait(strTitleClassName, "", 30);
			Thread.sleep(1000);
			x.controlFocus(strTitleClassName, "", "Edit1");
			Thread.sleep(1000);
			x.ControlSetText(strTitleClassName, "", "Edit1", valuetoType);
			Thread.sleep(1000);
			x.controlClick(strTitleClassName, "", "Button1");
			Thread.sleep(2000);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			Log.info("AutoIt configuration exception");
			return false;
		}

	}*/

	// To launch url
	public WebDriver launchURL(WebDriver driver, String valuetoType) throws Exception {
		Log.info(valuetoType+" is getting launched");	
		Environment.URL = valuetoType;
		// System.out.println("Const Browser:"+Constant.Report_Browser);
		// System.out.println("URl launchin.."+valuetoType);

		/*if (DriverScriptRef.driver != null) {
			try{
				driver.close();
				driver = null;

			}
			catch(Exception e){
				e.printStackTrace();
			}

		}*/
		

		driver.get(valuetoType);		
		return driver;
		// System.out.println("in launch..local driver"+driver+"  global "+DriverScriptRef.driver);
		//return true;

	}

	// closing the browser
	/*public boolean closeBrowser(WebDriver driver) {
		try{
		driver.close();
		driver = null;
		Environment.setDriver(driver);
		}
		catch(Exception e){
			driver = null;
			Environment.setDriver(driver);
		}
		return true;
	}
*/
	
	public WebDriver closeBrowser(WebDriver driver) {
		driver.close();
		return null;	
	}
	
	// Verify if a object exists
	public boolean verifyExists(WebDriver driver, String identifyBy,
			String locator) {

		WebElement we = webElement(driver, identifyBy, locator);

		if (we == null) {

			Log.info("Found" + identifyBy + " Locator-> " + locator);
			return false;

		} else {
			Log.info("Found" + identifyBy + " Locator-> " + locator);
			return true;

		}

	}

	// Verify value from an object
	public boolean verify(WebDriver driver, String identifyBy, String locator,
			String value) {
		String rtn = null;
		rtn = getValue(driver, identifyBy, locator);
		System.out.println("Return obj value==="+rtn.trim());
		System.out.println("Expected value==="+value.trim());
		if (rtn.trim().equals(value.trim())) {
			Log.info("Value Matched -> Actual Value:" + rtn
					+ " and Expected Value:" + value);
			return true;
		} else {
			Log.info("Value Did Not Match -> Actual Value:" + rtn
					+ " and Expected Value:" + value);
			return false;
		}
	}

	// Get value from an object
	public String getValue(WebDriver driver, String identifyBy, String locator) {
		String value = null;
		WebElement we = webElement(driver, identifyBy, locator);
				
		if (we == null) {
			return "";
		} else {
			Log.info("Found-> " + identifyBy + " using Locator-> " + locator
					+ " and got Value-> " + value);
			String strValue = "";
			try {
				strValue = we.getText();
			} catch (Exception e) {
				e.printStackTrace();

				try {
					strValue = we.getAttribute("value");
				} catch (Exception e1) {
					strValue = "";
				}
			}
			if (strValue.contentEquals("")) {

				try {
					strValue = we.getAttribute("value");
				} catch (Exception e) {
					strValue = "";
				}

			}
			return strValue;

		}

	}

	// Select radio button
	public boolean selectRadiobutton(WebDriver driver, String identifyBy,
			String locator) {

		WebElement we = webElement(driver, identifyBy, locator);
		if (we == null) {
			Log.info("Not Found-> " + identifyBy + " using Locator-> "
					+ locator);
			return false;
		} else {
			Log.info("Found-> " + identifyBy + " using Locator-> " + locator);
			we.click();
			Log.info("Clicked on -> " + identifyBy + " Locator-> " + locator);
			return true;
		}

	}

	// Select check box
	public boolean selectCheckbox(WebDriver driver, String identifyBy,
			String locator, String checkFlag) {

		WebElement we = webElement(driver, identifyBy, locator);

		if (we == null) {
			Log.info("Not Found-> " + identifyBy + " using Locator-> "
					+ locator);
			return false;
		} else {
			if ((checkFlag).equalsIgnoreCase("ON")) {
				if (!(driver.findElement(By.xpath(locator)).isSelected())) {
					driver.findElement(By.xpath(locator)).click();
				}
			}

			Log.info("Found-> " + identifyBy + " using Locator-> " + locator);
			return true;
		}
	}

	// Unselect check box
	public boolean unselectCheckbox(WebDriver driver, String identifyBy,
			String locator, String checkFlag) {

		WebElement we = webElement(driver, identifyBy, locator);

		if (we == null) {
			Log.info("Not Found-> " + identifyBy + " using Locator-> "
					+ locator);
			return false;
		} else {
			if ((checkFlag).equalsIgnoreCase("OFF")) {
				if (!(driver.findElement(By.xpath(locator)).isSelected())) {
					driver.findElement(By.xpath(locator)).click();
				}
			}

			Log.info("Found-> " + identifyBy + " using Locator-> " + locator);
			return true;
		}
	}

	// Select dropdown value
	public boolean selectValue(WebDriver driver, String identifyBy,
			String locator, String valToBeSelected) {

		WebElement we = webElement(driver, identifyBy, locator);

		if (we == null) {
			return false;
		} else {
			List<WebElement> options = we.findElements(By.tagName("option"));
			for (WebElement option : options) {
				if (valToBeSelected.equalsIgnoreCase(option.getText())) {
					option.click();
					Log.info("Found-> " + identifyBy + " using Locator-> "
							+ locator);
					return true;
				}
			}

			return false;
		}
	}

	// Scroll to bottom of a page
	public boolean scrollToBottom(WebDriver driver) {
		((JavascriptExecutor) driver)
		.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		return true;
	}

	// Scroll to a element on a page
	public boolean scrollTo(WebDriver driver, WebElement element) {
		((JavascriptExecutor) driver).executeScript(
				"arguments[0].scrollIntoView();", element);
		return true;
	}

	// Close an JavaScript alert
	public boolean alertClose(WebDriver driver) {
		Alert alert = driver.switchTo().alert();
		alert.dismiss();
		return true;
	}

	// Accept an JavaScript alert
	public boolean alertAccept(WebDriver driver) {
		Alert alert = driver.switchTo().alert();
		alert.accept();
		return true;
	}

	// Get text from an JavaScript alert
	public boolean alertGetText(WebDriver driver, String strAlertMsg) {
		Alert alert = driver.switchTo().alert();
		String strActualAlert = alert.getText();
		if (strActualAlert.contentEquals(strAlertMsg)) {
			return true;
		}
		return false;
	}

	// verifies the image completely is loaded or not
	public boolean verifyImageLoad(WebDriver driver, String identifyBy,
			String locator) {
		WebElement we = webElement(driver, identifyBy, locator);
		if (we == null) {
			Log.info("Not Found-> " + identifyBy + " using Locator-> "
					+ locator);
			return false;
		} else {
			Log.info("Element Found -> " + identifyBy + " using Locator-> "
					+ locator);
			Log.info("Verifying the image completely loaded-> " + identifyBy
					+ " using Locator-> " + locator);

			Boolean ImagePresent = (Boolean) ((JavascriptExecutor) driver)
					.executeScript(
							"return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0",
							we);
			if (!ImagePresent) {
				Log.info("Image completely loaded-> " + identifyBy
						+ " using Locator-> " + locator);
				return true;
			} else {
				Log.info("Image Not completely loaded-> " + identifyBy
						+ " using Locator-> " + locator);
				return false;
			}

		}

	}
	
	//Fluent wait until an element is found
	public boolean waitUntilElementFound(WebDriver driver, final String identifyBy, final String locator, String maxTimeOut){
		try{
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
				.withTimeout(Integer.parseInt(maxTimeOut), TimeUnit.SECONDS)
				.pollingEvery(1, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class);
		WebElement we = wait.until(new Function<WebDriver,WebElement>() {
			public WebElement apply(WebDriver driver) {
				return webElement(driver, identifyBy, locator);
			}
		});
		if(we == null)
			return false;
		else
			return true;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return true;
	}

}