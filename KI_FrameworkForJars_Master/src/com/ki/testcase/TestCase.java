package com.ki.testcase;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.ki.driverscript.DriverScriptRef;
import com.ki.driverscript.Environment;
import com.ki.interactions.Interactions2;
import com.ki.objectrepository.ObjectRepository;
import com.ki.utils.DBUtil;
import com.ki.utils.ExcelUtil;
import com.ki.utils.Log;
import com.ki.utils.ScreenShotUtils;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

@Test
public class TestCase {

	public boolean bTestCaseFlag = false;
	public boolean bTestStepFlag = false;
	public boolean bScreenShotForThisStep = false;
	//public WebDriver driver = null;
	

	// Run test cases
	public boolean defaultTestCase(WebDriver driver, String screen,
			String workbook, ExtentTest Report, String strBrowser)
					throws Exception {

		// Initialize the paths for TestData and ScreenShots.
		String tcPath = Environment.Path_TestData + Environment.File_TestData;
		String ShPath = Environment.Path_Screenshots;
		
		String tcWbk = workbook;
		boolean bStepFailureContinue = true;
		
		try {
			// Initialize screenshot driver
			ScreenShotUtils Sh = new ScreenShotUtils();

			// Open list of test cases to run.
			ExcelUtil tcExl = new ExcelUtil(tcPath, tcWbk);
			int tcrw = tcExl.getRowCount();
			// Loop through the test cases
			for (int tc = 1; tc < tcrw; tc++) {
				// Thread.sleep(1000);

				// Get test case data from excel
				ExcelUtil tcExl1 = new ExcelUtil(tcPath, tcWbk);
				
				if (bStepFailureContinue) {
					String tcno = tcExl1.getCellDataString2(tc, 0); //Test case no
					String tcaction = tcExl1.getCellDataString2(tc, 1); //Action to perform
					String tcobject = tcExl1.getCellDataString2(tc, 2); //Object type
					String tcobjname = tcExl1.getCellDataString2(tc, 3); //Name of object
					String tcvalue = tcExl1.getCellDataString2(tc, 4); //Object value to enter or check
					String strORName = tcExl1.getCellDataString2(tc, 5); //Object value to enter or check
	
					//This is to handle the image verification based on the testcase sheet. High priority would be on environment file
					if(!Environment.isbScreenShotForAllTheSteps()){
						String strDoTheImageCaptureIsNeeded = tcExl1.getCellDataString2(tc, 6); // Object
						bScreenShotForThisStep = false;
						if(strDoTheImageCaptureIsNeeded.trim().toLowerCase().contentEquals("yes"))
							bScreenShotForThisStep = true;
						else
							bScreenShotForThisStep = false;
					}
					
					// This is to identify if the value mentioned in the xls is
					// static or dynamic.If the values starts with # then it gets
					// from the assocated value
					if (tcvalue.startsWith("#")) {
						String strKey = tcvalue.substring(1);
						tcvalue = Environment.getDynamicValue(strKey);
					}
	
					//verify if the value is SQL command
					else if(tcvalue.toLowerCase().startsWith("sql:")){
						String strQuery = tcvalue.substring(4);
						if(strQuery.contains("#")){
	
							HashMap<String, String> hSqlKeyValues = new HashMap<>();
							Pattern r =Pattern.compile("(#[A-Za-z0-9]{1,})");
							// Now create matcher object.
							Matcher m = r.matcher(strQuery);
							while(m.find()) {
								String strKey = m.group(0);
								String strValue=Environment.getDynamicValue(strKey.substring(1));
								hSqlKeyValues.put(strKey, strValue);
							}
	
							Iterator it = hSqlKeyValues.entrySet().iterator();
							while (it.hasNext()) {
								Map.Entry pair = (Map.Entry)it.next();
								strQuery=strQuery.replaceAll(pair.getKey().toString(), pair.getValue().toString());
								it.remove(); // avoids a ConcurrentModificationException
							}
	
							hSqlKeyValues.clear();
	
						}
						DBUtil db= new DBUtil();
						db.setConnection();
						db.loadClass();
						tcvalue=db.getDBQueryResult(strQuery);
						
	
					}
	
					//This is used to reset the contents if any frame was set ealier
	
					setDefaultContent(driver);
	
	
					//This is used to switch to frames as and when there is a frame noticed in the screen
					String strFrameName = ObjectRepository.getFrameNameOfScreen(strORName);
					if(!strFrameName.trim().contentEquals("")){
						swithToFrame(driver,strFrameName);
					}
					// Get Locator Type and Locator from object repository for the
					// object using name
					String tctype = ObjectRepository.getORdata(strORName,
							tcobjname, "LocatorType");
					String tcrefrence = ObjectRepository.getORdata(strORName,
							tcobjname, "Locator");
	
					// Log the test case number
					if (!tcno.equals("")) {
	
						Log.lineBreak();
						Log.info("Execution started for test step: " + tcno);
						Log.lineBreak();
						Report.log(LogStatus.INFO, tcno);
	
					}
	
					Interactions2 itr = new Interactions2();
					String strActionName = tcaction.toUpperCase();
					// System.out.println("Action Name is:"+strActionName);
	
					switch (strActionName) {
						case "CLICK":
							bTestStepFlag = itr.click(driver, tctype,
									tcrefrence);
							if(bTestStepFlag)
								Report.log(LogStatus.PASS, "Clicked the button: "+tcobjname+" present in "+strORName+" page");
							else
								Report.log(LogStatus.FAIL, "Failed to click the button: "+tcobjname+" present in "+strORName+" page");
							break;
						case "CLICKLINK":
							bTestStepFlag = itr.clickLink(driver, tctype,
									tcrefrence);
							if(bTestStepFlag)
								Report.log(LogStatus.PASS, "Clicked the link: "+tcobjname+" present in "+strORName+" page");
							else
								Report.log(LogStatus.FAIL, "Failed to click the link: "+tcobjname+" present in "+strORName+" page");
							break;
						case "ENTERVALUE":
							bTestStepFlag = itr.enterValue(driver, tctype,
									tcrefrence, tcvalue);
							if(bTestStepFlag)
								Report.log(LogStatus.PASS, "Entered the value: "+tcvalue+" in textbox: "+tcobjname+" present in "+strORName+" page");
							else
								Report.log(LogStatus.FAIL, "Failed to enter the value: "+tcvalue+" in textbox: "+tcobjname+" present in "+strORName+" page");
							break;
						case "LAUNCHURL":
							String url ="";
							//if(tcvalue.trim().equals(""))
							//url = Environment.getURL();														
							driver = itr.launchURL(driver, tcvalue);							
							if(driver!=null){
								bTestStepFlag =true;
								Report.log(LogStatus.PASS, "Launched the URL: "+tcvalue);
							}
							else{
								bTestStepFlag =false;
								Report.log(LogStatus.FAIL, "Failed to launch the URL: "+tcvalue);
							}
							break;
						case "CLOSEBROWSER":
							driver= itr.closeBrowser(driver);
							if(driver!= null){
								bTestStepFlag = false;
								Report.log(LogStatus.PASS, "Browser is closed successfully");
							}
							else{
								bTestStepFlag =true;
								Report.log(LogStatus.FAIL, "Failed to close the browser");
							}
							break;
						case "VERIFYEXISTS":
							bTestStepFlag = itr.verifyExists(driver, tctype,
									tcrefrence);
							if(bTestStepFlag)
								Report.log(LogStatus.PASS, "Located the element: "+tcobjname+" present in "+strORName+" page");
							else
								Report.log(LogStatus.FAIL, "Failed to locate the element: "+tcobjname+" present in "+strORName+" page");
							break;
						case "VERIFYNOTEXISTS":
							bTestStepFlag = itr.verifyExists(driver, tctype,
									tcrefrence);
							if(!bTestStepFlag)
								Report.log(LogStatus.PASS, "Failed to locate the element: "+tcobjname+" present in "+strORName+" page as expected");
							else
								Report.log(LogStatus.FAIL, "Located the element: "+tcobjname+" present in "+strORName+" page not as expected");
							break;
						case "SELECTVALUE":
							bTestStepFlag = itr.selectValue(driver, tctype,
									tcrefrence, tcvalue);
							if(bTestStepFlag)
								Report.log(LogStatus.PASS, "Selected the value: "+tcvalue+" in dropdown: "+tcobjname+" present in "+strORName+" page");
							else
								Report.log(LogStatus.FAIL, "Failed to select the value: "+tcvalue+" in dropdown: "+tcobjname+" present in "+strORName+" page");
							break;
						case "VERIFY":
							bTestStepFlag = itr.verify(driver, tctype, tcrefrence,
									tcvalue);
							if(bTestStepFlag)
								Report.log(LogStatus.PASS, "Object: "+tcobjname+"'s value is matched with expected value: "+tcvalue+" in "+strORName+" page");
							else
								Report.log(LogStatus.FAIL, "Object: "+tcobjname+"'s value is mismatched with expected value: "+tcvalue+" in "+strORName+" page");
							break;
						case "SELECTRADIOBUTTON":
							bTestStepFlag = itr.selectRadiobutton(driver,
									tctype, tcrefrence);
							if(bTestStepFlag)
								Report.log(LogStatus.PASS, "Selected the radio button: "+tcobjname+" present in "+strORName+" page");
							else
								Report.log(LogStatus.FAIL, "Failed to select the radio button: "+tcobjname+" present in "+strORName+" page");
							break;
						case "SELECTCHECKBOX":
							bTestStepFlag = itr.selectCheckbox(driver, tctype,
									tcrefrence, "ON");
							if(bTestStepFlag)
								Report.log(LogStatus.PASS, "Selected the checkbox: "+tcobjname+" present in "+strORName+" page");
							else
								Report.log(LogStatus.FAIL, "Failed to select the checkbox: "+tcobjname+" present in "+strORName+" page");
							break;
						case "UNSELECTCHECKBOX":
							bTestStepFlag = itr.unselectCheckbox(driver,
									tctype, tcrefrence, "OFF");
							if(bTestStepFlag)
								Report.log(LogStatus.PASS, "Deselected the checkbox: "+tcobjname+" present in "+strORName+" page");
							else
								Report.log(LogStatus.FAIL, "Failed to deselect the checkbox: "+tcobjname+" present in "+strORName+" page");
							break;
						case "SCROLLTOBOTTOM":
							bTestStepFlag = itr.scrollToBottom(driver);
							if(bTestStepFlag)
								Report.log(LogStatus.PASS, "Scrolled to the bottom of "+strORName+" page");
							else
								Report.log(LogStatus.FAIL, "Failed to Scroll to the bottom of "+strORName+" page");
							break;
						case "ALERTCLOSE":
							bTestStepFlag = itr.alertAccept(driver);
							if(bTestStepFlag)
								Report.log(LogStatus.PASS, "Alert is closed");
							else
								Report.log(LogStatus.FAIL, "Failed to close the alert");
							break;
						case "ALERTACCEPT":
							bTestStepFlag = itr.alertAccept(driver);
							if(bTestStepFlag)
								Report.log(LogStatus.PASS, "Alert is accepted");
							else
								Report.log(LogStatus.FAIL, "Failed to accept the alert");
							break;
						case "ALERTGETTEXT":
							bTestStepFlag = itr.alertGetText(driver, tcvalue);
							if(bTestStepFlag)
								Report.log(LogStatus.PASS, "Alert's text is matched with expected value: "+tcvalue);
							else
								Report.log(LogStatus.FAIL, "Alert's text is mismatched with expected value: "+tcvalue);
							break;
		
							//This is used to get the value from the application and stores it into the variable mentioned under the value coulumn
						case "GETVALUEANDSTORE":
							String strValueInApp = itr.getValue(driver, tctype,
									tcrefrence);								
							Environment.setValue(tcvalue, strValueInApp);
							bTestStepFlag = true;
							if(bTestStepFlag)
								Report.log(LogStatus.PASS, "Object: "+tcobjname+"'s value: "+strValueInApp+" of "+strORName+" page is stored in the variable: "+tcvalue);
							else
								Report.log(LogStatus.FAIL, "Failed to store object: "+tcobjname+"'s value: "+strValueInApp+" of "+strORName+" page in the variable: "+tcvalue);
							break;
							
						case "ENTERSTOREDVALUE":
							bTestStepFlag = itr.enterValue(driver, tctype,
									tcrefrence, Environment.getDynamicValue(tcvalue));
							if(bTestStepFlag)
								Report.log(LogStatus.PASS, "Entered the value: "+Environment.getDynamicValue(tcvalue)+" in textbox: "+tcobjname+" present in "+strORName+" page");
							else
								Report.log(LogStatus.FAIL, "Failed to enter the value: "+Environment.getDynamicValue(tcvalue)+" in textbox: "+tcobjname+" present in "+strORName+" page");
							break;
							
						case "VERIFYSTOREDVALUE":
							bTestStepFlag = itr.verify(driver, tctype,
									tcrefrence, Environment.getDynamicValue(tcvalue));
							if(bTestStepFlag)
								Report.log(LogStatus.PASS, "Stored value: "+Environment.getDynamicValue(tcvalue)+" is matched with object's: "+tcobjname+" value present in "+strORName+" page");
							else
								Report.log(LogStatus.FAIL, "Stored value: "+Environment.getDynamicValue(tcvalue)+" in mismatched with object's: "+tcobjname+" value present in "+strORName+" page");
							break;
		
							//This is used to store any temp value associated with it.
						case "STORE":
							Environment.setValue(tcobjname, tcvalue);
							bTestStepFlag = true;
							if(bTestStepFlag)
								Report.log(LogStatus.PASS, "Object: "+tcobjname+"'s value of "+strORName+" page is loaded with the value: "+tcvalue);
							else
								Report.log(LogStatus.FAIL, "Object: "+tcobjname+"'s value of "+strORName+" page is failed to load with the value: "+tcvalue);
							break;
		
							//This is used to verify if the image gets loaded or not
						case "VERIFYIMAGELOAD":
							bTestStepFlag = itr.verifyImageLoad(driver,
									tctype, tcrefrence);
							if(bTestStepFlag)
								Report.log(LogStatus.PASS, "Object: "+tcobjname+"'s image load is complete");
							else
								Report.log(LogStatus.FAIL, "Object: "+tcobjname+"'s image load failed");
							break;
		
						/*case "FILEUPLOAD":
							bTestStepFlag = itr.fileUpload(driver, tctype,
									tcrefrence, tcvalue);
							if(bTestStepFlag)
								Report.log(LogStatus.PASS, "Object: "+tcobjname+"'s file upload is complete");
							else
								Report.log(LogStatus.FAIL, "Object: "+tcobjname+"'s file upload failed");
							break;*/
							
						case "WAITUNTILELEMENTFOUND":
							bTestStepFlag = itr.waitUntilElementFound(driver, tctype,
									tcrefrence, tcvalue);
							if(bTestStepFlag)
								Report.log(LogStatus.PASS, "Object: "+tcobjname+" is found during maximum fluent wait of: "+tcvalue+" seconds");
							else
								Report.log(LogStatus.FAIL, "Object: "+tcobjname+"is not found during maximum fluent wait of: "+tcvalue+" seconds");
							break;							
						default:
							if (!strActionName.contentEquals("")) {
								try {
									// Execute the test cases
									Class<?> cls = Class.forName("Actions");
									Object obj = cls.newInstance();
			
									// Get the methods
									Method method = cls.getMethod(tcaction,
											new Class[] { WebDriver.class,
											String.class, String.class,
											String.class });
			
									// Call the Execute method
									bTestStepFlag = (boolean) method.invoke(
											obj, driver, strORName, tcobjname, tcvalue);
									if(bTestStepFlag)
										Report.log(LogStatus.PASS, "Test step: "+tcaction+" execution is as expected");
									else
										Report.log(LogStatus.FAIL, "Test step: "+tcaction+" execution is not as expected");
								} catch (Exception e) {
									bTestStepFlag = false;
								}
							}
							break;
					}
					
					if(Environment.bScreenShotForAllTheSteps || bScreenShotForThisStep)
						takeScreenShot(Report, Sh, driver, ShPath, screen, strActionName, bTestStepFlag);
					if(bTestStepFlag)
						bTestCaseFlag = true;
					else
						bTestCaseFlag = false;
					if(Environment.isbStepFailToBeContinued())
						bStepFailureContinue = true;
					else
						bStepFailureContinue = bTestCaseFlag;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(bTestCaseFlag)
			return true;
		else
			return false;
	}

	//this method is used to get the screenshot as and when needed.
	public static void takeScreenShot(ExtentTest Report, ScreenShotUtils Sh,
			WebDriver driver, String ShPath, String Fname,
			String strActionName, boolean result) {
		String Location = "", relativeLocation = "";		
		try {
			Location = Sh.takeScreenShot(driver, ShPath, Fname);
			System.out.println("Location===="+Location);
			String[] Locations = Location.split("/");
			relativeLocation = ".";
			for(int i=Locations.length-3; i<=Locations.length-1; i++)
				relativeLocation+="/"+Locations[i];			
			// Log if test case passed or failed.
			Log.info("Test Case Status = " + ((result) ? "Passed" : "Failed"));
			Log.info("Test Case  Evidence captured as:" + Location);
			// Report
			/*Report.log(((result) ? LogStatus.PASS : LogStatus.FAIL),
					"Test Case Status: " + ((result) ? "Passed" : "Failed"));
			 */
			String getfile[] = relativeLocation.split("/");
			System.out.println("getfile[2]==="+getfile[3]);
			Report.log(LogStatus.INFO,
					"Evidence:" + Report.addScreenCapture(relativeLocation));			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void takeScreenShotRobot(ExtentTest Report, ScreenShotUtils Sh,
			WebDriver driver, String ShPath, String Fname,
			String strActionName, boolean result) {
		String Location = "", relativeLocation = "";
		try {
			Location = Sh.takeScreenShotRobot(driver, ShPath, Fname);
			String[] Locations = Location.split("/");
			relativeLocation = ".";
			for(int i=Locations.length-3; i<=Locations.length-1; i++)
				relativeLocation+="/"+Locations[i];
			// Log if test case passed or failed.
			Log.info("Test Case Status = " + ((result) ? "Passed" : "Failed"));
			Log.info("Test Case  Evidence captured as:" + Location);
			// Report
			/*Report.log(((result) ? LogStatus.PASS : LogStatus.FAIL),
					"Test Case Status: " + ((result) ? "Passed" : "Failed"));
			 */
			Report.log(LogStatus.INFO,
					"Evidence:" + Report.addScreenCapture(relativeLocation));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setDefaultContent(WebDriver driver){
		try{
			if(driver !=null){
				driver.switchTo().defaultContent();
			}
		}
		catch(Exception e){
			Log.warn("Issue in setting the default content");
			//e.printStackTrace();
		}
	}

	//This is used to switch the frame
	public boolean swithToFrame(WebDriver driver,String strFrameNames){
		try{

			String strFrames[]=	strFrameNames.split(",");
			for (int i = 0; i < strFrames.length; i++) {

				try{
					//This is to switch frame based on index.
					int strFrameOrderLocation = Integer.parseInt(strFrames[i]);
					WebDriverWait wait = new WebDriverWait(driver,10);
					wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(strFrameOrderLocation));
				}
				catch(NumberFormatException e){
					try{
 
						WebDriverWait wait = new WebDriverWait(driver,10);
						wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(strFrames[i]));

					}
					catch(Exception e1){
						Log.info("Issue in identifying the frame");
						e1.printStackTrace();
					}
				}
				catch(Exception e){
					Log.info("Issue in identifying the element with index");
					e.printStackTrace();
				}

			}
			return true;
		}
		catch(Exception e){
			Log.info("Issue in setting the default content");
			e.printStackTrace();
			return false;
		}
	}
}
