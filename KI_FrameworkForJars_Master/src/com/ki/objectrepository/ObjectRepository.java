package com.ki.objectrepository;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.ki.driverscript.Environment;
import com.ki.utils.Log;

public class ObjectRepository {

	public static String getFrameNameOfScreen(String screen){

		String eleDesc = null;
		try {
			// Create a DOM Document object
			Document doc2 = createDocument(Environment.Path_OR);
			NodeList nList = doc2.getElementsByTagName("Screen");

			// Loop through all the screens
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					// System.out.println("\nCurrent Element :"+nNode.getNodeName());
					// System.out.println("Screen Name= : "+eElement.getAttribute("Name"));
					if (eElement.getAttribute("Name").equals(screen)) {

						String strFrameNames = eElement.getAttribute("frame");
						if(strFrameNames.trim().contentEquals("")){
							return "";
						}
						//Log.info("Element is in the Frame "+strFrameNames);
						System.out.println("Element is in the Frame "+strFrameNames);
						return strFrameNames;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Return object repository information

		return "";


	}

	public static String getORdata(String screen, String objName, String eleType) {
		String eleDesc = null;
		try {
			// Create a DOM Document object
			Document doc2 = createDocument(Environment.Path_OR);
			NodeList nList = doc2.getElementsByTagName("Screen");

			// Loop through all the screens
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					// System.out.println("\nCurrent Element :"+nNode.getNodeName());
					// System.out.println("Screen Name= : "+eElement.getAttribute("Name"));
					if (eElement.getAttribute("Name").equals(screen)) {
						NodeList nList2 = nNode.getChildNodes();
						// Loop through all the objects of the screen
						for (int temp2 = 0; temp2 < nList2.getLength(); temp2++) {
							Node nNode2 = nList2.item(temp2);
							if (nNode2.getNodeType() == Node.ELEMENT_NODE) {
								Element eElement2 = (Element) nNode2;
								if (eElement2.getAttribute("Name").equals(
										objName)) {
									// System.out.println("\nCurrent Element :"+
									// nNode2.getNodeName());
									// System.out.println("Element Name : "+eElement2.getAttribute("Name"));
									if (eleType.equals("LocatorType")) {
										eleDesc = eElement2
												.getElementsByTagName(
														"LocatorType").item(0)
														.getTextContent();
									} else if (eleType.equals("Locator")) {
										eleDesc = eElement2
												.getElementsByTagName("Locator")
												.item(0).getTextContent();
									}
									break;
								}
							}
						}
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Return object repository information
		return eleDesc;
	}

	// Create a DOM Document object from a file
	private static Document createDocument(String fileName) {
		Document doc = null;
		try {
			// Get the file
			File xmlFile = new File(fileName);
			// Create document builder factory
			DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
			doc = docBuilder.parse(xmlFile);
			doc.getDocumentElement().normalize();
			doc.setXmlStandalone(true);
		}// Exception handling
		catch (IOException ioe) {
			Log.error("Couldn't open file: " + fileName);
			System.exit(1);
		} catch (SAXException se) {
			Log.error("Couldn't parse the XML file" + fileName);
			System.exit(1);
		} catch (ParserConfigurationException pce) {
			Log.error("Couldn't create a DocumentBuilder for" + fileName);
			System.exit(1);
		}
		// Return the Document object
		return doc;
	}


}