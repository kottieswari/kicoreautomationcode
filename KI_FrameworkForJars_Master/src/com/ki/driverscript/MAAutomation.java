package com.ki.driverscript;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;

import com.ki.driverscript.DriverScriptRef;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * This is used to execute the TestNg class 
 */
public class MAAutomation {

	public void startExecution() {
		
		try {
			TestListenerAdapter tla = new TestListenerAdapter();
			TestNG testng = new TestNG();

			testng.setTestClasses(new Class[] { DriverScriptRef.class });
			testng.addListener(tla);
			testng.run();
		}catch(Exception e){
			e.printStackTrace();
		}

	}

}