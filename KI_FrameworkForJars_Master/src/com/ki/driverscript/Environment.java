package com.ki.driverscript;

import java.util.HashMap;

import org.openqa.selenium.WebDriver;

import com.ki.driverscript.DriverScriptRef;
import com.ki.driverscript.Environment;
import com.ki.interactions.Interactions2;
import com.ki.testcase.TestCase;
import com.ki.utils.EnvUtil;
import com.ki.utils.Log;
import com.relevantcodes.extentreports.ExtentReports;

public class Environment {

	public static HashMap<String, String> hDynmaicEnvVariable = new HashMap<>();
	public static String URL ;
	public static String TestPath;

	public static String Path_TestData;
	public static String File_TestData;
	public static String WBook_TestExecute;
	public static String Path_Screenshots;
	public static String Path_OR;
	public static String Path_Report;
	public static String Execution_Browser;
	public static String Report_Browser;
	public static String IE_Driver_Path;
	public static String Chrome_Driver_Path;
	private static Interactions2 itr2;
	public static boolean bScreenShotForAllTheSteps=false;
	public static boolean bRecoveryScenarioExecution=false;
	public static String strRecoverSheetName="";
	public static String LogOption="";
	
	public static String DBType;
	public static String DBHostName;
	public static String DBPortNumber;
	public static String DBName;
	public static String DBUserName;
	public static String DBPassword;
	public static boolean DBWindowAuthentication=false;
	
	public static boolean bStepFailToBeContinued=false;
	
	public static boolean bMailToBeSentAfterExecution=false;
	public static String SmtpHost;
	public static String SmtpSSLPort;
	public static String SmtpEmailId;
	public static String SmtpEmailPassword;
	public static String EmailIdsToBeSent;

	public static String getEmailIdsToBeSent() {
		return EmailIdsToBeSent;
	}

	public static void setEmailIdsToBeSent(String emailIdsToBeSent) {
		EmailIdsToBeSent = emailIdsToBeSent;
	}

	public static void settingPaths() {
		Log.info("Setting all the environmental details" );
		EnvUtil env = new EnvUtil();
		env.loadAllProperties();

		String strTestPath = env.getProperty("TestPath");

		if (strTestPath.contentEquals("")) {
			Log.info("Not taking the path from the default folder. Please set it");
		} else {

			TestPath = strTestPath;
			Path_TestData = TestPath + "TestData//";
			//File_TestData = "TestData.xlsx";
			File_TestData = env.getProperty("TestDataFile");
			
			WBook_TestExecute = "TestExecute";
			Path_OR = TestPath + "TestData//OR.XML";
			Path_Report = TestPath + "Reports";
			
		}

		String strBrowser = env.getProperty("Browser");
		if (strBrowser.contentEquals("")) {
			Log.info("Please specify browser in environment file for test run");
		} else {
			Execution_Browser = strBrowser.trim();
			Report_Browser = strBrowser.trim();
		}
		
		String strURL = env.getProperty("URL");
		if (strURL.contentEquals("")) {
			Log.info("Launch URL will be picked from test scenario sheet");
		} else {
			URL = strURL.trim();
		}

		String strIEDriverLoacation = env.getProperty("webdriver.ie.driver");

		if (!strIEDriverLoacation.trim().contentEquals("")) {
			Log.info("IEDriverserver.exe file location was set in " + strIEDriverLoacation);
			IE_Driver_Path = strIEDriverLoacation;
		}

		String strChromeDriverLoacation = env.getProperty("webdriver.chrome.driver");
		if (!strChromeDriverLoacation.trim().contentEquals("")) {
			Log.info("ChromeDriverserver.exe file location was set in " + strChromeDriverLoacation);
			Chrome_Driver_Path = strChromeDriverLoacation;
		}

		String bDoTheImagesNeedsTobeLoaded= env.getProperty("ScreenShotForAllTheSteps");
		if(bDoTheImagesNeedsTobeLoaded.toLowerCase().contentEquals("yes")){
			Log.info("Enabling flag for image capture for all the actions");
			setbScreenShotForAllTheSteps(true);
		}
		
		String bDoRecovoeryScenarioNeede = env.getProperty("RecoverScenario");

		if (bDoRecovoeryScenarioNeede.trim().toLowerCase().contentEquals("yes")) {
			Log.info("Recovery scenarios were set. In the event of failure, recovery scenarios will be executed");
			setbRecoveryScenarioExecution(true);
		} 		
		
		String strDoRecovoeryScenarioSheet = env.getProperty("RecoveryScenarioSheetName");

		if (!strDoRecovoeryScenarioSheet.trim().contentEquals("")) {
			setStrRecoverSheetName(strDoRecovoeryScenarioSheet);	
		}
		
		String strLogOption = env.getProperty("LogOption");
		if(!strLogOption.trim().contentEquals("")) {
			LogOption = strLogOption.replaceAll("\\s", "");
		}
		
		setDBType(env.getProperty("DBType"));
		setDBHostName(env.getProperty("DBHostName"));
		setDBName(env.getProperty("DBName"));
		setDBPortNumber(env.getProperty("DBPortNumber"));
		setDBUserName(env.getProperty("DBUserName"));
		setDBPassword(env.getProperty("DBPassword"));
		
		setSmtpEmailId(env.getProperty("SmtpEmailId"));
		setSmtpEmailPassword(env.getProperty("SmtpEmailPassword"));
		setSmtpHost(env.getProperty("SmtpHost"));
		setSmtpSSLPort(env.getProperty("SmtpSSLPort"));
		setEmailIdsToBeSent(env.getProperty("EmailIdsToBeSent"));
		
		String bMailToBeSentAfterExecution = env.getProperty("MailToBeSentAfterExecution");

		if (bMailToBeSentAfterExecution.trim().toLowerCase().contentEquals("yes")) {
			Log.info("Email Triggering");
			setbDoEmailToBeTriggered(true);
		} 
		
		
		String strTestScriptExecutionContinueInFailure = env.getProperty("TestScriptExecutionContinueInFailure");

		if (strTestScriptExecutionContinueInFailure.trim().toLowerCase().contentEquals("yes")) {
			Log.info("Execution continues to next step execution");
			setbStepFailToBeContinued(true);
		} 	
			
		// Report_Browser = "FireFox";
	}

	public static boolean bDoEmailToBeTriggered=false;
	public static boolean isbDoEmailToBeTriggered() {
		return bDoEmailToBeTriggered;
	}

	public static void setbDoEmailToBeTriggered(boolean bDoEmailToBeTriggered) {
		Environment.bDoEmailToBeTriggered = bDoEmailToBeTriggered;
	}

	public static String getSmtpHost() {
		return SmtpHost;
	}

	public static void setSmtpHost(String smtpHost) {
		SmtpHost = smtpHost;
	}

	public static String getSmtpSSLPort() {
		return SmtpSSLPort;
	}

	public static void setSmtpSSLPort(String smtpSSLPort) {
		SmtpSSLPort = smtpSSLPort;
	}

	public static String getSmtpEmailId() {
		return SmtpEmailId;
	}

	public static void setSmtpEmailId(String smtpEmailId) {
		SmtpEmailId = smtpEmailId;
	}

	public static String getSmtpEmailPassword() {
		return SmtpEmailPassword;
	}

	public static void setSmtpEmailPassword(String smtpEmailPassword) {
		SmtpEmailPassword = smtpEmailPassword;
	}

	public static String getDBType() {
		return DBType;
	}

	public static void setDBType(String dBType) {
		DBType = dBType;
	}

	public static String getURL() {
		return URL;
	}

	public static String getTestPath() {
		return TestPath;
	}

	public static String getPath_TestData() {
		return Path_TestData;
	}

	public static String getFile_TestData() {
		return File_TestData;
	}

	public static String getWBook_TestExecute() {
		return WBook_TestExecute;
	}

	public static String getPath_Screenshots() {
		return Path_Screenshots;
	}

	public static String getPath_OR() {
		return Path_OR;
	}

	public static String getPath_Report() {
		return Path_Report;
	}

	public static String getReport_Browser() {
		return Report_Browser;
	}

	public static String getIE_Driver_Path() {
		return IE_Driver_Path;
	}

	public static String getChrome_Driver_Path() {
		return Chrome_Driver_Path;
	}

	public static void setDriver(WebDriver driver) {
		DriverScriptRef.driver = driver;
	}

	/*public static void setExtent(ExtentReports extent) {
		DriverScriptRef.extent = extent;
	}*/

	public static WebDriver getDriver() {
		return DriverScriptRef.driver;
	}

	/*public static ExtentReports getExtent() {
		return DriverScriptRef.extent;
	}*/

	public static void setValue(String strKey,String strValue){
		System.out.println("strKey - strValue === "+strKey+ "-"+strValue);
		hDynmaicEnvVariable.put(strKey, strValue);
	}
	
	public static String getDynamicValue(String strKey){
		return hDynmaicEnvVariable.get(strKey);
	}
	
	public static Interactions2 getITRReferenceObject(){
		
		if(itr2 != null){
			 itr2 = new Interactions2();
			}
		return itr2;
	}
	public static boolean isbScreenShotForAllTheSteps() {
		return bScreenShotForAllTheSteps;
	}

	public static void setbScreenShotForAllTheSteps(
			boolean bScreenShotForAllTheSteps) {
		Environment.bScreenShotForAllTheSteps = bScreenShotForAllTheSteps;
	}

	public static boolean isbRecoveryScenarioExecution() {
		return bRecoveryScenarioExecution;
	}

	public static void setbRecoveryScenarioExecution(
			boolean bRecoveryScenarioExecution) {
		Environment.bRecoveryScenarioExecution = bRecoveryScenarioExecution;
	}

	public static String getStrRecoverSheetName() {
		return strRecoverSheetName;
	}

	public static void setStrRecoverSheetName(String strRecoverSheetName) {
		Environment.strRecoverSheetName = strRecoverSheetName;
	}

	
	public static String getDBHostName() {
		return DBHostName;
	}

	public static void setDBHostName(String dBHostName) {
		DBHostName = dBHostName;
	}

	public static String getDBPortNumber() {
		return DBPortNumber;
	}

	public static void setDBPortNumber(String dBPortNumber) {
		DBPortNumber = dBPortNumber;
	}

	public static String getDBName() {
		return DBName;
	}

	public static void setDBName(String dBName) {
		DBName = dBName;
	}

	public static String getDBUserName() {
		return DBUserName;
	}

	public static void setDBUserName(String dBUserName) {
		DBUserName = dBUserName;
	}

	public static String getDBPassword() {
		return DBPassword;
	}

	public static void setDBPassword(String dBPassword) {
		DBPassword = dBPassword;
	}

	public static boolean isDBWindowAuthentication() {
		return DBWindowAuthentication;
	}

	public static void setDBWindowAuthentication(boolean dBWindowAuthentication) {
		DBWindowAuthentication = dBWindowAuthentication;
	}
	
	
	public static boolean isbStepFailToBeContinued() {
		return bStepFailToBeContinued;
	}

	public static void setbStepFailToBeContinued(boolean bStepFailToBeContinued) {
		Environment.bStepFailToBeContinued = bStepFailToBeContinued;
	}

}

