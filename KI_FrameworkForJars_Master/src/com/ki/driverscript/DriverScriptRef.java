package com.ki.driverscript;



import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.MarionetteDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

import java.io.File;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.ki.driverscript.Environment;
import com.ki.interactions.Interactions2;
import com.ki.testcase.TestCase;
//import com.ki.utils.EmailUtil;
import com.ki.utils.ExcelUtil;
import com.ki.utils.Log;
import com.ki.utils.Utility;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class DriverScriptRef {

	public static  ExtentReports extent;
	public static WebDriver driver = null;
	private static Boolean bTestScenarioResult;
	private static String strTimeOfExecution="";
	public static ExtentTest Report;

	@BeforeMethod
	public void beforeMethod() throws Exception {

		// Log4j configuration settings
		//DOMConfigurator.configure("Log4j.xml");
		Environment.settingPaths();
		Date dNow = new Date();
		//SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
		SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy_hh'H'_mm'M'_ss'S'");
		strTimeOfExecution = ft.format(dNow);
		
		//Log.configureLog4j(Environment.getTestPath()+"Logs/",strTimeOfExecution);
		
		//Create a folder for reporting
		Environment.Path_Report = Environment.Path_Report+"/TestRun_"+strTimeOfExecution;
		File reportDir = new File(Environment.Path_Report);
		reportDir.mkdir();
		
		Log.configureLog4j(Environment.Path_Report+"/",strTimeOfExecution);
		
		//Create a screenshot folder inside report folder
		Environment.Path_Screenshots = Environment.Path_Report+"/Screenshots";
		File screenDir = new File(Environment.Path_Screenshots);
		screenDir.mkdir();
		
		// Configure the reporting and create an instance, By default this displays OS,UserName,Javaversion,HostName details in the report
		extent = new ExtentReports(Environment.Path_Report+"/ExecutionReport"+strTimeOfExecution+".html",true);
		// To get additional info. If we are running in windows 10 virtual machine, since virtual box is setup on Windows 8.1 so it gives the OS name as windows 8.1
		// To advoid this below piece of code is added.
		//String OS = System.getProperty("os.name");
		//if(OS.equals("Windows 8.1")){
			//extent.addSystemInfo("OS","Windows 10");
		//}
		extent.addSystemInfo("Browser",Environment.Execution_Browser.toUpperCase());
		
	
	}

	@Test
	public void main() throws Exception {

		// Initialize the paths for TestData and ScreenShots.
		String ePath = Environment.Path_TestData + Environment.File_TestData;
		String eWbk = Environment.WBook_TestExecute;
        System.out.println("epath==="+ePath+"ewbk==="+eWbk);
		try {
			// Open list of test scripts to run.
			ExcelUtil eExl = new ExcelUtil(ePath, eWbk);
			int erw = eExl.getRowCount();
			
			//Launching the browser
			driver = launchBrowser(Environment.Execution_Browser);            
			com.ki.testcase.TestCase tc = new TestCase();
			// Loop through the test scripts
			for (int j = 1; j < erw; j++) {
				ExcelUtil eExl1 = new ExcelUtil(ePath, eWbk);
				String tcase = eExl1.getCellDataString2(j, 0); //Test Case
				String tdesc =  eExl1.getCellDataString2(j, 1); //Description
				String tname = tcase+"_"+tdesc; //Test Name
				String tbrowser = eExl1.getCellDataString2(j, 1); //Test Browser
				String trun = eExl1.getCellDataString2(j, 2); //Execute Yes or No
				String twbk = eExl1.getCellDataString2(j, 3); //Test Workbook
				System.out.println("tbrowser==="+tbrowser);
				// Run the tests if execute option is set to Yes
				if (trun.equals("Yes")) {
					// Open the URL in the browser mentioned
					// driver = Utils.Utility.LaunchBrowser(;tbrowser,
					// Constant.URL);
					Environment.Path_Screenshots = Environment.Path_Screenshots+"/"+tcase+"/";
					File screenDir1 = new File(Environment.Path_Screenshots);
					screenDir1.mkdir();
					// Starting test
					Report = extent.startTest(tname);
					Log.startTestCase(tname);
					//tc = new com.ki.testcase.TestCase();
					bTestScenarioResult = tc.defaultTestCase(driver, tname,twbk, Report, tbrowser);
					testCaseUpdate(bTestScenarioResult, Report, tname);
					//recoveryScenarioUpdate(tc, Report, tbrowser, "Recovery Scenario", "tscript", "Default", trun);
					Log.endTestCase(tname);
					extent.endTest(Report);
					Environment.Path_Screenshots = Environment.Path_Report+"/Screenshots";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void afterMethod() {
		try {
			//Closing browser at the end of execution
			quitBrowser();
			Log.info("Report generated in the location:"+Environment.Path_Report + "/Report"+strTimeOfExecution+".html");
			// Create report
			extent.flush();
			Thread.sleep(5000);
			// Open the report in the browser mentioned after the test run
			// TODO: Parameterize browser and Report path
			Log.info("Results Location is:"+ Environment.Path_Report);
			WebDriver driver = Utility.launchBrowserWithURL(
					com.ki.driverscript.Environment.Report_Browser, "file:///" + Environment.Path_Report+"/ExecutionReport"+strTimeOfExecution+".html");
			
			clickOnDashboardLinkinExtendReport();
			
			if(Environment.isbDoEmailToBeTriggered()){
				Log.info("Sending Mail");
				//EmailUtil.sendingMail(Environment.getSmtpHost(), Environment.getSmtpSSLPort(), Environment.getSmtpEmailId(), Environment.getSmtpEmailPassword(), Environment.getEmailIdsToBeSent(),strTimeOfExecution,driver);
			}
			else{
				Log.info("Email trigger flag is not set. Please set, if required");
			}
			
		} catch (Exception e) {
			e.getMessage();
		}
	}
	
	public static WebDriver quitBrowser() {
		try {
			driver.close();
		} catch (Exception e) {
			Log.error(e.getMessage());
		}
		return null;
	}

	@SuppressWarnings("deprecation")
	public static WebDriver launchBrowser(String sBrowser)
			throws Exception {
		try {
			if (sBrowser.toUpperCase().equals("FIREFOX")) {
				/*File pathToBinary = new File("C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe");//Use the firefox.exe local path
                FirefoxBinary ffBinary = new FirefoxBinary(pathToBinary);
                // Open FireFox
                FirefoxProfile profile = new FirefoxProfile();
                // Accept Untrusted Certificates
                profile.setAcceptUntrustedCertificates(true);
                // Initialize Firefox driver
                driver = new FirefoxDriver(ffBinary,profile);
                Log.info("New driver instantiated for browser:" + sBrowser);
                com.ki.driverscript.DriverScriptRef.driver.manage().window()
                              .maximize();	*/				
				System.setProperty("webdriver.gecko.driver", "TestData/geckodriver.exe");
				DesiredCapabilities capabilities = new DesiredCapabilities();
					capabilities.setCapability("marionette", true);
					capabilities.setCapability(FirefoxDriver.BINARY, "C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe");
					//driver = new MarionetteDriver(capabilities); 
					driver = new FirefoxDriver(capabilities);
					Log.info("New driver instantiated for browser:" + sBrowser);
					Thread.sleep(5000);					
					com.ki.driverscript.DriverScriptRef.driver.manage().window().maximize();

			} else if (sBrowser.toUpperCase().equals("IE")) {
				// Open IE
				System.setProperty("webdriver.ie.driver",
						Environment.IE_Driver_Path);
				DesiredCapabilities capability = DesiredCapabilities
						.internetExplorer();
				capability
						.setCapability(
								InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
								true);
				
				com.ki.driverscript.DriverScriptRef.driver = new InternetExplorerDriver(
						capability);
				Log.info("New driver instantiated for browser:" + sBrowser);
				com.ki.driverscript.DriverScriptRef.driver.manage()
						.timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				Log.info("Implicit wait applied on the driver for 30 seconds");
				com.ki.driverscript.DriverScriptRef.driver.manage().window()
						.maximize();
			} else if (sBrowser.toUpperCase().equals("CHROME")) {
				// Open Chrome
				System.setProperty("webdriver.chrome.driver",
						Environment.Chrome_Driver_Path);
				
				ChromeOptions options = new ChromeOptions();
                DesiredCapabilities caps = new DesiredCapabilities();
                caps.setCapability(ChromeOptions.CAPABILITY, options);
                
                options.addArguments("disable-infobars");

				com.ki.driverscript.DriverScriptRef.driver = new ChromeDriver(options);
				Log.info("New driver instantiated for browser:" + sBrowser);
				//com.ki.driverscript.DriverScriptRef.driver.manage()
						//.timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				//Log.info("Implicit wait applied on the driver for 30 seconds");
				com.ki.driverscript.DriverScriptRef.driver.manage().window()
						.maximize();
			}
		} catch (Exception e) {
			Log.error(e.getMessage());
		}
		return com.ki.driverscript.DriverScriptRef.driver;
	}
	
    public void clickOnDashboardLinkinExtendReport()
    {
    	try{
     
    		Interactions2 it = new Interactions2();
    		it.click(driver, "className", "dashboard-view");
    	}
    	catch(Exception e){
    		Log.info("Dashboard Clicking issue");
    		e.printStackTrace();
    	}
    }
	
	public void testCaseUpdate(boolean result, ExtentTest report, String tname) {
		Log.info("Result is "+result);
		if (result) {
			report.log(LogStatus.PASS, tname+" - Overall Status : PASS");
		} else {
			report.log(LogStatus.FAIL, tname+" - Overall Status : FAIL");
		}
	}

	public void recoveryScenarioUpdate(TestCase tc,ExtentTest Report,String tbrowser,String tname,String tscript,String tmethod,String trun){

		Log.info("In the recovery scenario"+bTestScenarioResult);
		//Recovery Scenario Handling
		if(!bTestScenarioResult){
			extent.endTest(Report);
			if(Environment.isbRecoveryScenarioExecution()){
				Log.info("Recovery Scenario Enabled");
				if(Environment.getStrRecoverSheetName().trim().contentEquals("")){
					Log.info("Recovery Scenario Enabled but the Recovery Sheet name is not enabled.");
					Log.info("Recover Sheet Name key value in the Environment.properties file should be RecoveryScenariSheetName");
				}
				else{
					Log.info("Executing the recovery scenario from the Sheet :"+Environment.getStrRecoverSheetName());
					boolean bRecoverResult = false;
					try {
						ExtentTest extentTestForFailure = extent.startTest("Test Name-> " + tname,	"Test Script-> " + tscript + "<BR>Test Method-> "
								+ tmethod + "<BR>Execute(Y/N)-> " + trun);
						bRecoverResult = tc.defaultTestCase(driver, tname,Environment.getStrRecoverSheetName(), extentTestForFailure, tbrowser);
						if(bRecoverResult){
							extentTestForFailure.log(LogStatus.PASS, "Test Name-> " + tname);
						}
						else{
							extentTestForFailure.log(LogStatus.FAIL, "Test Name-> " + tname);
						}
						
						extent.endTest(extentTestForFailure);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					//ExtentTest extentTestForFailure = extent.startTest("Test Name-> " + "Recovery Scenario","Test Method-> Recovery  Sheet"+"<BR>Execute(Y/N)-> Yes" );
					
					Log.info("Recover Scenario Result is "+bRecoverResult );
					//testCaseUpdate(bRecoverResult, Report, tname);	
				}
			}
		}
		else{

			extent.endTest(Report);
		}

	}

}
