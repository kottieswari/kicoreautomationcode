package com.ki.utils;

import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.ki.utils.Log;

public class ScreenShotUtils {

	public static String name = null;
	public String takeScreenShot(WebDriver driver, String Path, String Fname)
			throws Exception {
		
		try{
			driver.switchTo().defaultContent();
			Log.info("Taking the screenshot");
		}
		catch(Exception e){
			Log.info("Exception in switching to default content");
			e.printStackTrace();
		}
		// Capture screenshot
		File screenshot = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		String[] splitFname = Fname.split("_");
		/*String name = Path + "" + Fname + " " + getFileNameDate() + ".jpg"; -- 
		Commented because Fname is the combination of both TC# and description. So, if we use Fname while naming
		file it becomes very big file which causes some problem while copying the captured screenshot to the file*/
		//String name = Path + "" + Fname + " " + getFileNameDate()
		String name = Path + "" + splitFname[0] + " " + getFileNameDate() + ".jpg";
		
		// Copies the captured screenshot to the file created
		FileUtils.copyFile(screenshot, new File(name));
		return name;
	}
	
	public String takeScreenShotRobot(WebDriver driver, String Path,
			String Fname) throws Exception {

		try {
			driver.switchTo().defaultContent();
			Log.info("Taking the screenshot");
		} catch (Exception e) {
			Log.info("Exception in switching to default content");
			e.printStackTrace();
		}

		BufferedImage image = null;
		image = new Robot().createScreenCapture(new Rectangle(Toolkit
				.getDefaultToolkit().getScreenSize()));
		String[] splitFname = Fname.split("_");
		/*String name = Path + "" + Fname + " " + getFileNameDate() + ".jpg"; -- 
		Commented because Fname is the combination of both TC# and description. So, if we use Fname while naming
		file it becomes very big file which causes some problem while copying the captured screenshot to the file*/
		//name = Path + "" + Fname + " " + getFileNameDate() + ".jpg";
		name = Path + "" + splitFname[0] + " " + getFileNameDate() + ".jpg";
		ImageIO.write(image, "jpg", new File(name));
		return name;
	}

	private String getFileNameDate() throws Exception {
		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
		String fname = ft.format(dNow);
		return fname;
	}
}
