package com.ki.utils;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.MarionetteDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ki.driverscript.Environment;
import com.ki.utils.Log;

public class Utility {
	// public static WebDriver driver = null;
	
	@SuppressWarnings("deprecation")
	public static WebDriver launchBrowserWithURL(String sBrowser, String sURL)
			throws Exception {
		try {
			if (sBrowser.toUpperCase().equals("FIREFOX")) {
				// Open FireFox
				/*FirefoxProfile profile = new FirefoxProfile();
				profile.setEnableNativeEvents(true);

				com.ki.driverscript.DriverScriptRef.driver = new FirefoxDriver(
						profile);
				Log.info("New driver instantiated for browser:" + sBrowser);
				com.ki.driverscript.DriverScriptRef.driver.manage()
						.timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				Log.info("Implicit wait applied on the driver for 30 seconds");
				com.ki.driverscript.DriverScriptRef.driver.get(sURL);
				com.ki.driverscript.DriverScriptRef.driver.manage().window()
						.maximize();
				Log.info("Web application launched successfully:" + sURL);*/	
				
				
				System.setProperty("webdriver.gecko.driver", "C:/Griffintown/KI_Workspace/KI_Framework/TestData/geckodriver.exe");
				DesiredCapabilities capabilities = new DesiredCapabilities();
					capabilities.setCapability("marionette", true);
					capabilities.setCapability(FirefoxDriver.BINARY, "C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe");
					com.ki.driverscript.DriverScriptRef.driver = new MarionetteDriver(capabilities); 
					Log.info("New driver instantiated for browser:" + sBrowser);
					com.ki.driverscript.DriverScriptRef.driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
					com.ki.driverscript.DriverScriptRef.driver.get(sURL);
					com.ki.driverscript.DriverScriptRef.driver.manage().window()
							.maximize();
					Log.info("Web application launched successfully:" + sURL);					
				
				
			} else if (sBrowser.toUpperCase().equals("IE")) {
				// Open IE

				System.setProperty("webdriver.ie.driver",
						Environment.IE_Driver_Path);
				DesiredCapabilities capability = DesiredCapabilities
						.internetExplorer();
				capability
						.setCapability(
								InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
								true);

				com.ki.driverscript.DriverScriptRef.driver = new InternetExplorerDriver(
						capability);
				Log.info("New driver instantiated for browser:" + sBrowser);
				com.ki.driverscript.DriverScriptRef.driver.manage()
						.timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				Log.info("Implicit wait applied on the driver for 30 seconds");
				System.out.println(sURL);
				com.ki.driverscript.DriverScriptRef.driver.get(sURL);
				com.ki.driverscript.DriverScriptRef.driver.manage().window()
						.maximize();
				Log.info("Web application launched successfully:" + sURL);
			} else if (sBrowser.toUpperCase().equals("CHROME")) {
				// Open Chrome
				System.setProperty("webdriver.chrome.driver",
						Environment.Chrome_Driver_Path);
				com.ki.driverscript.DriverScriptRef.driver = new ChromeDriver();
				Log.info("New driver instantiated for browser:" + sBrowser);
				com.ki.driverscript.DriverScriptRef.driver.manage()
						.timeouts().implicitlyWait(30, TimeUnit.SECONDS);
				Log.info("Implicit wait applied on the driver for 30 seconds");
				com.ki.driverscript.DriverScriptRef.driver.get(sURL);
				com.ki.driverscript.DriverScriptRef.driver.manage().window()
						.maximize();
				Log.info("Web application launched successfully:" + sURL);
			}
		} catch (Exception e) {
			Log.error("Class Utility | Method LaunchBrowser | Exception desc : "
					+ e.getMessage());
		}
		return com.ki.driverscript.DriverScriptRef.driver;
	}

	// Wait for element to load
	public static void waitForElement(WebElement element) {
		WebDriverWait wait = new WebDriverWait(
				com.ki.driverscript.DriverScriptRef.driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

}