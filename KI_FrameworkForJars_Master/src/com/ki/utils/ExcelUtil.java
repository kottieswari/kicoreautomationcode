package com.ki.utils;

import java.io.*;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.*;

import com.ki.utils.Log;

public class ExcelUtil {
	private static XSSFSheet ExcelWSheet;
	private static XSSFWorkbook ExcelWBook;
	private static XSSFCell Cell;
	private static XSSFRow Row;

	// Constructor to connect to the Excel with sheetname and Path
	public ExcelUtil(String Path, String SheetName) throws Exception {
		try {
			// Open the Excel file
			FileInputStream ExcelFile = new FileInputStream(Path);
			// Access the required test data sheet
			ExcelWBook = new XSSFWorkbook(ExcelFile);
			ExcelWSheet = ExcelWBook.getSheet(SheetName);
		} catch (Exception e) {
			throw (e);
		}
	}

	// This method is to set the rowcount of the excel.
	public int getRowCount() throws Exception {
		try {
			return ExcelWSheet.getPhysicalNumberOfRows();
		} catch (Exception e) {
			throw (e);

		}
	}

	// This method to get the data and get the value as strings.
	public String getCellDataString(int RowNum, int ColNum) throws Exception {
		//try {
			Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
			String CellData = Cell.getStringCellValue();
			System.out.println("getCellDataString--CellData==="+CellData);
			return CellData;
//		} catch (Exception e) {
//			e.printStackTrace();
//			return "Errors in Getting Cell Data";
//		}
	}

	// This method to get the data and get the value as strings.
	public String getCellDataString2(int RowNum, int ColNum) throws Exception {
		//try {
			DataFormatter formatter = new DataFormatter(); // creating formatter
															// using the default
															// locale
			Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
			String CellData = formatter.formatCellValue(Cell); // Returns the
																// formatted
																// value of a
																// cell as a
																// String
																// regardless of
																// the cell type
			return CellData;
//		} catch (Exception e) {
//			//e.printStackTrace();
//			return "Errors in Getting Cell Data";
//		}
	}

	// This method to get the data and get the value as number.
	public double getCellDataNumber(int RowNum, int ColNum) throws Exception {
		try {
			Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
			double CellData = Cell.getNumericCellValue();
			return CellData;
		} catch (Exception e) {
			return 000.00;
		}
	}

	// This method is to write in the Excel cell, Row num, Col num and path are
	// the parameters
	@SuppressWarnings("static-access")
	public void setCellData(String Result, int RowNum, int ColNum, String Path)
			throws Exception {
		try {
			Row = ExcelWSheet.getRow(RowNum);
			Cell = Row.getCell(ColNum, Row.RETURN_BLANK_AS_NULL);
			if (Cell == null) {
				Cell = Row.createCell(ColNum);
				Cell.setCellValue(Result);
			} else {
				Cell.setCellValue(Result);
			}
			// Constant variables Test Data path and Test Data file name
			FileOutputStream fileOut = new FileOutputStream(Path);
			ExcelWBook.write(fileOut);
			fileOut.flush();
			fileOut.close();
		} catch (Exception e) {
			throw (e);
		}
	}

	// To get the data based on the columnName and the rowNumber
	public String getCellData(String strColumnName, int rowNumber)
			throws Exception {

		int noOfColumns = ExcelWSheet.getRow(0).getPhysicalNumberOfCells();

		for (int i = 0; i < noOfColumns; i++) {
			if (getCellDataString2(0, i).trim().contentEquals(strColumnName)) {
				return getCellDataString(rowNumber, i);
			}
		}
		Log.info("Coloumn Name " + strColumnName
				+ " is not present as coloumn Name");
		return null;
	}

}