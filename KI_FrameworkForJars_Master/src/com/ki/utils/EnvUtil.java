package com.ki.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class EnvUtil {

	private static Properties prop = new Properties();

	public void loadAllProperties() {

		InputStream input = null;

		try {

			input = new FileInputStream("Environment.properties");

			// load a properties file
			prop.load(input);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public String getProperty(String strPropertyName) {

		String propVal = prop.getProperty(strPropertyName);
		if (propVal == null) {
			return "";
		} else {
			return propVal;
		}
	}

	
}
