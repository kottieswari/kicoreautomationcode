package com.ki.utils;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.ki.driverscript.Environment;
import com.ki.utils.DBUtil;
import com.ki.utils.EnvUtil;
import com.ki.utils.Log;
//THis class is used for working with the Database quries
public class DBUtil {

	/*private static 	Connection con = null;
	private static CallableStatement cstmt = null;
	private static ResultSet rs = null;
	private static String strConnectionURL;*/
	String url;
	Connection con = null;
	Statement stmt = null;
	CallableStatement cstmt = null;
	ResultSet rs = null;

	//Connection would be establised
	public void setConnection(){

		if(Environment.getDBType().trim().toLowerCase().contains("sqlserver")){

			url="jdbc:sqlserver://"+Environment.getDBHostName()+":"+Environment.getDBPortNumber()+";databaseName="+Environment.getDBName();

		}

	}

	//Associated Driver class would be loaded 
	public void loadClass() throws ClassNotFoundException{
		if(Environment.getDBType().trim().toLowerCase().contains("sqlserver")){

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		}

	}

	public static void main(String[] args) throws SQLException, ClassNotFoundException{

		EnvUtil env = new EnvUtil();
		env.loadAllProperties();
		Environment environmet = new Environment();

		environmet.setDBType(env.getProperty("DBType"));
		environmet.setDBHostName(env.getProperty("DBHostName"));
		environmet.setDBName(env.getProperty("DBName"));
		environmet.setDBPortNumber(env.getProperty("DBPortNumber"));
		environmet.setDBUserName(env.getProperty("DBUserName"));
		environmet.setDBPassword(env.getProperty("DBPassword"));


		DBUtil du = new DBUtil();
		du.setConnection();
		du.loadClass();
		String SQL = "select Top 5 zipcode from Plattform_DB.dbo.AcceptanceArea with (nolock) where Campaignid=6683 and curriculumid=482178 and LocationID=32011";
		du.getDBQueryResult(SQL);
	}

	//To get the value from the query.The query should be one coloumn and one cell reference.
	public String getDBQueryResult(String strSQLCommand) throws SQLException{

		try {
			// Establish the connection.
			con = DriverManager.getConnection(url, Environment.getDBUserName(), Environment.getDBPassword()); 
			// Create and execute an SQL statement that returns some data.
			Log.info("Query Executing is :"+strSQLCommand);
			/*if(strSQLCommand.trim().toLowerCase().startsWith("call ")){
				cstmt = con.prepareCall("{call dbo.uspGetEmployeeManagers(?)}");
				cstmt.setInt(1, 50);
				rs = cstmt.executeQuery();
			}
			else{*/
			stmt = con.createStatement();
			rs = stmt.executeQuery(strSQLCommand);
			//}
			// Iterate through the data in the result set and display it.
			String strVal =getdataFromDB(rs);

			rs.close();
			con.close();
			return strVal;

		}

		// Handle any errors that may have occurred.
		catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (Exception e) {
				}
			if (stmt != null)
				try {
					stmt.close();
				} catch (Exception e) {
				}
			if (con != null)
				try {
					con.close();
				} catch (Exception e) {
				}
		}
		return "";

	}

	//To get perticular coloumn data.
	private String getdataFromDB(ResultSet rs){

		try {
			while (rs.next()){
				Log.info("Value is:"+rs.getString(1));
				return rs.getString(1);
			}
			return "";
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}

	}


	private static void displayRow(ResultSet rs) {
		try {
			while (rs.next()) {
				System.out.println(rs.getString(1) );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
