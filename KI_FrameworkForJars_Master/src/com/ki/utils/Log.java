package com.ki.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

import com.ki.driverscript.DriverScriptRef;
import com.ki.utils.Log;

import freemarker.core.Environment;

public class Log  {

	// Initialize Log4j logs
	private static Logger Log = Logger.getLogger(Log.class.getName());

	
	public static void configureLog4j(String strPath,String strTimeOfExecution){
		  String PATTERN = "%d{yyyy-MM-dd HH:mm:ss} %-5p %c{1}:%L - %m%n";
		  //System.out.println(strPath+File.separator+"Log"+strTimeOfExecution+".log");
		 
		  if(com.ki.driverscript.Environment.LogOption.equalsIgnoreCase("Console")||com.ki.driverscript.Environment.LogOption.equalsIgnoreCase("Console&File")) {
			  ConsoleAppender console = new ConsoleAppender(); //create appender
			  //configure the appender
			  console.setLayout(new PatternLayout(PATTERN)); 
			  console.setThreshold(Level.INFO);
			  console.activateOptions();
			  //add appender to any Logger (here is root)
			  Logger.getRootLogger().addAppender(console); 
		  }

		  if(com.ki.driverscript.Environment.LogOption.equalsIgnoreCase("File")||com.ki.driverscript.Environment.LogOption.equalsIgnoreCase("Console&File")) {	
			  fileCreate(strPath+"Log"+strTimeOfExecution+".log");
			  FileAppender fa = new FileAppender();
			  fa.setName("FileLogger");
			  fa.setFile(strPath+"Log"+strTimeOfExecution+".log");
			  fa.setLayout(new PatternLayout(PATTERN));
			  fa.setThreshold(Level.INFO);
			  fa.setAppend(true);
			  fa.activateOptions();

			  //add appender to any Logger (here is root)
			  Logger.getRootLogger().addAppender(fa);
			  //repeat with all other desired appenders
		  }
	}
	
	private static void fileCreate(String strFileName){
	try{	
		new FileOutputStream(strFileName, false).close();
	}
	catch(Exception e){
		e.printStackTrace();
	}
	}
	
	// Print log for the beginning of the test case
	public static void startTestCase(String sTestCaseName) {
		Log.info("**************************************************************************************************");
		Log.info("$$$$$$$$$$$$$$$$$$$$$                 START: "
				+ sTestCaseName + "       $$$$$$$$$$$$$$$$$$$$$$$$$");
		Log.info("**************************************************************************************************");
		
	}

	
	// Print log for the ending of the test case
	public static void endTestCase(String sTestCaseName) {
		Log.info("**************************************************************************************************");
		Log.info("XXXXXXXXXXXXXXXXXXXXXXX                 END: "
				+ sTestCaseName + "       XXXXXXXXXXXXXXXXXXXXXXXXX");
		Log.info("**************************************************************************************************");
	}

	// Print line break
	public static void lineBreak() {
		Log.info("**************************************************************************************************");
	}

	// Default logging methods
	public static void info(String message) {
		Log.info(message);
	}

	public static void warn(String message) {
		Log.warn(message);
	}

	public static void error(String message) {
		Log.error(message);
	}

	public static void fatal(String message) {
		Log.fatal(message);
	}

	public static void debug(String message) {
		Log.debug(message);
	}

}
