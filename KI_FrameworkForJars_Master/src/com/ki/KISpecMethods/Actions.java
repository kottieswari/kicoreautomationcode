package com.ki.KISpecMethods;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.ki.driverscript.Environment;
import com.ki.interactions.Interactions2;
import com.ki.testcase.TestCase;
import com.ki.utils.Log;
import com.ki.utils.ScreenShotUtils;
import com.relevantcodes.extentreports.LogStatus;


public class Actions extends ProjectSpecific {
	// Variable Declaration
			static Interactions2 itr2 = new Interactions2();
	/**
	 * Function for check the control type
	 * 
	 * @param driver
	 * @param screenName
	 *            from the input sheet
	 * @param ObjectName
	 *            for the respective object from the input sheet
	 */

	public boolean checkControltype(WebDriver driver, String screen,
			String object, String value) {
		try {
			WebElement element = itr2.getWebElement(driver, screen, object);
			String type = element.getAttribute("type");			
			if (type.equalsIgnoreCase("button")) {			
				com.ki.driverscript.DriverScriptRef.Report.log(
						LogStatus.PASS, object + " is a TextBox");
			} else if(type.equalsIgnoreCase("button")){
				com.ki.driverscript.DriverScriptRef.Report.log(
						LogStatus.PASS, object + " is a Button");
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}
	
	//get control type
	public static boolean getControltype(WebDriver driver, String screenName,
			String ObjectName, String value) {
		try {
			WebElement ele = itr2.getWebElement(driver, screenName, ObjectName);
			String type = ele.getAttribute("type");
				System.out.println("11111===="+type);
			

		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	
	// Enter value in text box
		public boolean enterValueDynamic(WebDriver driver, String screen, String object, String valuetoType) throws InterruptedException {			
					String timestamp = new Timestamp(System.currentTimeMillis()).toString();
					timestamp = timestamp.replace('-', '0');
					timestamp = timestamp.replace(':', '0');
					timestamp = timestamp.replace('.', '0');
					timestamp = timestamp.replace(' ', '0');

					//WebElement we = webElement(driver, identifyBy, locator);
					WebElement we = itr2.getWebElement(driver, screen, object);	
					if (we == null) {
						Log.info("Not Found-> " + object);
						return false;
					} else {
						Log.info("Found-> " + object);
						we.clear();
						we.sendKeys(valuetoType+timestamp);
						Thread.sleep(2000);
						Log.info("Entered Value on -> " +object+ " Locator");
						return true;
					}

				}
	

}